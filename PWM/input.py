import RPi.GPIO as GPIO
import time
import os

output = 18
GPIO.setmode (GPIO.BCM)
GPIO.setup(output, GPIO.OUT)

freq = 1000 # 1000Hz frequency
p = GPIO.PWM(output, freq) # 1000Hz frequency

DUTYCYCLE = 0; # TURN ON RATIO = percentage

os.system("clear")

print("starting...")
p.start(DUTYCYCLE)

num = 1
while num > -1:
    num = float(input("type percentage\n"))
    
    if num > -1:
        p.ChangeDutyCycle(num)
        
    print("Percentage set: %d" % num)
    
print("...end")
GPIO.cleanup()
