import RPi.GPIO as GPIO
import time
import os

output = 18
GPIO.setmode (GPIO.BCM)
GPIO.setup(output, GPIO.OUT)


p =GPIO.PWM(output, 100) # 100Hz frequency

DUTYCYCLE = 30; # TURN ON RATIO = percentage

os.system("clear")

print("starting...")
p.start(DUTYCYCLE)

time.sleep(20)

    
print("...end")
GPIO.cleanup()
