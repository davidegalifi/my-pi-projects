import RPi.GPIO as GPIO
import time
import os

output = 18
GPIO.setmode (GPIO.BCM)
GPIO.setup(output, GPIO.OUT)


p = GPIO.PWM(output, 1) # 1Hz frequency = 1 cycles per second

DUTYCYCLE =10; # TURN ON RATIO = percentage

os.system("clear")

print("starting...")
p.start(DUTYCYCLE)

time.sleep(10)
    
print("...end")
GPIO.cleanup()
