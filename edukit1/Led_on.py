import RPi.GPIO as GPIO
import time
import os

led = 18

GPIO.setmode(GPIO.BCM)
GPIO.setup(led,GPIO.OUT)
os.system("clear")
print("Lights on")

GPIO.output(led, GPIO.HIGH)

time.sleep(3)

GPIO.cleanup()
