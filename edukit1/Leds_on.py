import RPi.GPIO as GPIO
import time
import os

led = 18
led2 = 23

GPIO.setmode(GPIO.BCM)
GPIO.setup(led,GPIO.OUT)
GPIO.setup(led2,GPIO.OUT)
os.system("clear")
print("Lights on")

GPIO.output(led, GPIO.HIGH)
GPIO.output(led2, GPIO.HIGH)

time.sleep(3)

GPIO.cleanup()
