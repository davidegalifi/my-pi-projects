import time
import pigpio

class driver:

    def set_speed(self, pulse):
        self.pulses[0] = pulse
        self.pulses[1] = pulse
        self.pulses[2] = pulse
        self.pulses[3] = pulse
        
        self.pi.set_servo_pulsewidth(self.M1, pulse)
        self.pi.set_servo_pulsewidth(self.M2, pulse)
        self.pi.set_servo_pulsewidth(self.M3, pulse)
        self.pi.set_servo_pulsewidth(self.M4, pulse)

    def inc_speed(self, inc):
        
        while inc > 0:
            if min(self.pulses) < 1100 and min(self.pulses) >= 1000:
                self.pulses[0] = 1100 + self.pitches[1] + self.pitches[2]
                self.pulses[1] = 1100 + self.pitches[1] + self.pitches[3]
                self.pulses[2] = 1100 + self.pitches[0] + self.pitches[3]
                self.pulses[3] = 1100 + self.pitches[0] + self.pitches[2]
                inc = 0

                self.pi.set_servo_pulsewidth(self.M1, self.pulses[0])
                self.pi.set_servo_pulsewidth(self.M2, self.pulses[1])
                self.pi.set_servo_pulsewidth(self.M3, self.pulses[2])
                self.pi.set_servo_pulsewidth(self.M4, self.pulses[3])

            else:
                self.pulses[0] = self.pulses[0] + 1
                self.pulses[1] = self.pulses[1] + 1
                self.pulses[2] = self.pulses[2] + 1
                self.pulses[3] = self.pulses[3] + 1

                self.pi.set_servo_pulsewidth(self.M1, self.pulses[0])
                self.pi.set_servo_pulsewidth(self.M2, self.pulses[1])
                self.pi.set_servo_pulsewidth(self.M3, self.pulses[2])
                self.pi.set_servo_pulsewidth(self.M4, self.pulses[3])

                time.sleep(0.01)
                inc = inc - 1
                
            self.print_status(self.pulses)

    def dec_speed(self, dec):
        
        while dec > 0:
            if min(self.pulses) <= 1100:
                self.set_speed(1000)
                dec = 0
            else:
                self.pulses[0] = self.pulses[0] - 1
                self.pulses[1] = self.pulses[1] - 1
                self.pulses[2] = self.pulses[2] - 1
                self.pulses[3] = self.pulses[3] - 1

                self.pi.set_servo_pulsewidth(self.M1, self.pulses[0])
                self.pi.set_servo_pulsewidth(self.M2, self.pulses[1])
                self.pi.set_servo_pulsewidth(self.M3, self.pulses[2])
                self.pi.set_servo_pulsewidth(self.M4, self.pulses[3])

                time.sleep(0.01)
                dec = dec - 1
                
            self.print_status(self.pulses)
            
        
    def off(self):
        while min(self.pulses) > 1000:
            self.dec_speed(50)
            time.sleep(0.001)
        self.set_speed(0)
        print("off!")
        self.pi.stop()

    def pitch_forward(self):
        self.pulses[2] = self.pulses[2] + 2
        self.pulses[3] = self.pulses[3] + 2

        self.pitches[0] = self.pitches[0] + 2
        
        self.pi.set_servo_pulsewidth(self.M3, self.pulses[2])
        self.pi.set_servo_pulsewidth(self.M4, self.pulses[3])
        
        self.print_status(self.pulses)

    def pitch_back(self):
        self.pulses[0] = self.pulses[0] + 2
        self.pulses[1] = self.pulses[1] + 2

        self.pitches[1] = self.pitches[1] + 2
        
        self.pi.set_servo_pulsewidth(self.M1, self.pulses[0])
        self.pi.set_servo_pulsewidth(self.M2, self.pulses[1])

        self.print_status(self.pulses)

    def pitch_right(self):
        self.pulses[0] = self.pulses[0] + 2
        self.pulses[3] = self.pulses[3] + 2

        self.pitches[2] = self.pitches[2] + 2
        
        self.pi.set_servo_pulsewidth(self.M1, self.pulses[0])
        self.pi.set_servo_pulsewidth(self.M4, self.pulses[3])

        self.print_status(self.pulses)

    def pitch_left(self):
        self.pulses[1] = self.pulses[1] + 2
        self.pulses[2] = self.pulses[2] + 2

        self.pitches[3] = self.pitches[3] + 2
        
        self.pi.set_servo_pulsewidth(self.M2, self.pulses[1])
        self.pi.set_servo_pulsewidth(self.M3, self.pulses[2])

        self.print_status(self.pulses)
        
    def print_status(self, pulses):
        print("---------------")
        print("| " + str(self.pulses[0])  + " | " + str(self.pulses[1]) + " |")
        print("---------------")
        print("| " + str(self.pulses[3])  + " | " + str(self.pulses[2]) + " |")
        print("---------------")

        
    
    def initialise(self):
        
        self.pi.set_PWM_frequency(self.M1, 1000)
        self.pi.set_PWM_frequency(self.M2, 1000)
        self.pi.set_PWM_frequency(self.M3, 1000)
        self.pi.set_PWM_frequency(self.M4, 1000)
        self.pi.set_PWM_range(self.M1, 1000)
        self.pi.set_PWM_range(self.M2, 1000)
        self.pi.set_PWM_range(self.M3, 1000)
        self.pi.set_PWM_range(self.M4, 1000)

        self.pitches = [0,0,0,0]
        
        self.starting = 1
        self.set_speed(0)
        
        input("press enter to initialise")
    
        print("initialisation...")

        time.sleep(2)
        self.set_speed(2000)
        time.sleep(3)
        self.set_speed(1000)
        
        print("ready")
        self.print_status(self.pulses)
        input("press enter to start")
        
    
    def __init__(self,pi, M1, M2, M3, M4):
        self.M1 = M1
        self.M2 = M2
        self.M3 = M3
        self.M4 = M4
        self.pi = pi
        self.pulses = [0,0,0,0]
