import time
import pigpio
import sys
import curses

PIN = 18


pi = pigpio.pi()

try:
    pi.set_PWM_frequency(PIN, 1000)
    pi.set_PWM_range(PIN, 1000)

    dutycycle = 155
    print("initialisation...")
    pi.set_PWM_dutycycle(PIN, dutycycle)

    dutycycle = 130
    pi.set_PWM_dutycycle(PIN, dutycycle)

    print("ready")

    x = 1
    while int(x) > 0:
        x = input() 
        print(x)
        pi.set_servo_pulsewidth(PIN, x)

    print("stopped")

    pulses = 500
    pi.set_servo_pulsewidth(PIN, pulses)
    pi.set_PWM_dutycycle(PIN, 0)
    pi.stop()     
    

    
except KeyboardInterrupt:
    print("you hit ctrl-c")
    pi.set_PWM_dutycycle(PIN, 0)
    pulses = 500
    pi.set_servo_pulsewidth(PIN, pulses)
    
    pi.stop()
