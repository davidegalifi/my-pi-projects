import time
import _thread
import pigpio
import keyboard
from rotation import *
from driver import *

M1 = 17
M2 = 18
M3 = 19
M4 = 20

pi = pigpio.pi()
driver = driver(pi, M1,M2,M3,M4)
pulses = 0
stop = 0
accelerometer = accelerometer()

def get_inc(value):
    return value

def adjust(rotation):
    x = rotation[0]
    y = rotation[1]
    
    if x < 0:
        print("pitch back")
    if x > 0:
        print("pitch forward")
    if y > 0:
        print("pitch left")
    if y < 0:
        print("pitch right")

def check_orientation():
    while stop == 0:
        rotation = accelerometer.get_orientation()
        print(str(rotation))
	
        adjust(rotation)

        time.sleep(0.01)

try:
    driver.initialise()
  
    driver.inc_speed(100)
    
    _thread.start_new_thread(check_orientation, ())
    while stop == 0:

        key = str(keyboard.getKey())
        
        if key[6:] == "[A'": #up
            driver.inc_speed(50)
        elif key[6:] == "[B'": #down
            driver.dec_speed(50)
        elif key == "b'w'":
            driver.pitch_forward()
        elif key == "b's'":
            driver.pitch_back()
        elif key == "b'a'":
            driver.pitch_left()
        elif key == "b'd'":
            driver.pitch_right()
        else:
            print(key)
            
    print("end loop")
    driver.off()


except KeyboardInterrupt:
    print("you hit ctrl-c")
    driver.off()
    
#except:
   # print("generic exception")
   # driver.off()    
    
    
