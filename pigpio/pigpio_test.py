import time
import pigpio
import keyboard

PIN = 18

pi = pigpio.pi()

try:
    pi.set_PWM_frequency(PIN, 1000)
    pi.set_PWM_range(PIN, 1000)

    dutycycle = 200
    print("initialisation...")
    pi.set_PWM_dutycycle(PIN, dutycycle)

    dutycycle = 130
    pi.set_PWM_dutycycle(PIN, dutycycle)

   
    while 1:
        x = str(keyboard.getKey())
        if x[6:] == "[A'":
            if dutycycle == 130:
                dutycycle = 140
            #elif dutycycle == 300:
             #   dutycycle = 300
            else:
                dutycycle = dutycycle + 1
        if x[6:] == "[B'":
            if (dutycycle <= 140):
                dutycycle = 130
            else:
                dutycycle = dutycycle - 1

        pi.set_PWM_dutycycle(PIN, dutycycle)
        print(dutycycle)
        
    pi.set_PWM_dutycycle(PIN, 0)
    pi.stop()

    
except KeyboardInterrupt:
    print("you hit ctrl-c")
    pi.set_PWM_dutycycle(PIN, 0)
    pi.stop()
