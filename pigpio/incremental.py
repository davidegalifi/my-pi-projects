import time
import pigpio
import sys
import curses

PIN = 20


pi = pigpio.pi()



try:
    pi.set_PWM_frequency(PIN, 1000)
    pi.set_PWM_range(PIN, 1000)

    dc = 155
    print("initialisation...")
    pi.set_PWM_dutycycle(PIN, dc)

    dc = 130
    pi.set_PWM_dutycycle(PIN, dc)

    input()
    import os
    import termios
    import tty

    forward = 1
    stop = 0
    while stop == 0:
        if forward == 1:
            if dc == 130:
                dc = 140
            if dc == 300:
                forward = 0
            else:
                dc = dc + 1
        else:
            if dc == 130:
                stop = 1
            else:
                dc = dc - 1

        pi.set_PWM_dutycycle(PIN, dc)
        print(dc)
        time.sleep(0.1)
        
    pi.set_PWM_dutycycle(PIN, 0)
    pi.stop()

    
except KeyboardInterrupt:
    print("you hit ctrl-c")
    pi.set_PWM_dutycycle(PIN, 0)
    pi.stop()
