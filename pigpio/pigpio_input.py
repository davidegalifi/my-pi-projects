import time
import pigpio
import sys
import curses

M1 = 20


pi = pigpio.pi()

try:
    pi.set_PWM_frequency(M1, 1000)
    pi.set_PWM_range(M1, 1000)

    
    time.sleep(1)

    pulses = 2000
    print("initialisation...")
    pi.set_servo_pulsewidth(M1, pulses)
    

    time.sleep(3)

    pulses = 1000
    pi.set_servo_pulsewidth(M1, pulses)


    print("ready")

    x = 1
    while int(x) > 0:
        x = input() 
        print(x)
        pi.set_servo_pulsewidth(M1, x)


    print("stopped")

    pulses = 500
    pi.set_servo_pulsewidth(M1, pulses)

    pi.stop()
    
except KeyboardInterrupt:
    print("you hit ctrl-c")
    pulses = 500
    pi.set_servo_pulsewidth(M1, pulses)

    pi.stop()
