import time
import pigpio
import keyboard
from driver import *

M1 = 17
M2 = 18
M3 = 19
M4 = 20

pi = pigpio.pi()
driver = driver(pi, M1,M2,M3,M4)
pulses = 0

try:
    driver.initialise()
    pulses = 1000
    
    forward = 1
    stop = 0
    step = 1200
    while stop == 0:
        if forward == 1:
            if pulses == 1000:
                pulses = 1100
            elif pulses == 2000:
                forward = 0
            elif pulses == step:
                print(pulses)
                key = str(keyboard.getKey())
                                
                if key[6:] == "[A'":
                    step = step + 50
                else:
                    step = step - 50
                    forward = 0
            else:
                pulses = pulses + 1
        else:
            if pulses == 1000:
                stop = 1
            elif pulses == step:
                print(pulses)
                key = str(keyboard.getKey())
                                
                if key[6:] == "[A'":
                    step = step + 50
                    forward = 1
                else:
                    step = step - 50
            else:
                pulses = pulses - 1

        driver.set_speed(pulses)
        
        #print(pulses)
        time.sleep(0.01)
        
    driver.set_speed(pulses)

    pulses = 0
    driver.set_speed(pulses)
    pi.stop()


except KeyboardInterrupt:
    print("you hit ctrl-c")

    while pulses > 1000:
            pulses = pulses - 3
            driver.set_speed(pulses)

            time.sleep(0.01)
            
    pulses = 0
    driver.set_speed(pulses)
    pi.stop()
##except:
##    print("generic exception")
##    pulses = 0
##    driver.set_speed(pulses)
##    pi.stop()
    
