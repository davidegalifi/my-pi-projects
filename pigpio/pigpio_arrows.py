import time
import pigpio
import keyboard

M1 = 17
M2 = 18
M3 = 19
M4 = 20

pi = pigpio.pi()


try:
    pi.set_PWM_frequency(M1, 1000)
    pi.set_PWM_frequency(M2, 1000)
    pi.set_PWM_frequency(M3, 1000)
    pi.set_PWM_frequency(M4, 1000)

    pi.set_PWM_range(M1, 1000)
    pi.set_PWM_range(M2, 1000)
    pi.set_PWM_range(M3, 1000)
    pi.set_PWM_range(M4, 1000)
    

    pulses = 500

    pi.set_servo_pulsewidth(M1, pulses)
    pi.set_servo_pulsewidth(M2, pulses)
    pi.set_servo_pulsewidth(M3, pulses)
    pi.set_servo_pulsewidth(M4, pulses)
    
    time.sleep(2)

    
    pulses = 2000
    print("initialisation...")
    
    pi.set_servo_pulsewidth(M1, pulses)
    pi.set_servo_pulsewidth(M2, pulses)
    pi.set_servo_pulsewidth(M3, pulses)
    pi.set_servo_pulsewidth(M4, pulses)
    

    time.sleep(3)

    pulses = 1000
    pi.set_servo_pulsewidth(M1, pulses)
    pi.set_servo_pulsewidth(M2, pulses)
    pi.set_servo_pulsewidth(M3, pulses)
    pi.set_servo_pulsewidth(M4, pulses)

    print("ready")


    while 1:
        x = str(keyboard.getKey())
        if x[6:] == "[A'":
            if(pulses == 1000):
                pulses = 1100
            else:
                pulses = pulses + 1
        if x[6:] == "[B'":
            pulses = pulses - 1

        pi.set_servo_pulsewidth(M1, pulses)
        pi.set_servo_pulsewidth(M2, pulses)
        pi.set_servo_pulsewidth(M3, pulses)
        pi.set_servo_pulsewidth(M4, pulses)
        print(pulses)
        
    print("stopped")

    pulses = 500
    pi.set_servo_pulsewidth(M1, pulses)
    pi.set_servo_pulsewidth(M2, pulses)
    pi.set_servo_pulsewidth(M3, pulses)
    pi.set_servo_pulsewidth(M4, pulses)
    pi.stop()

    
except KeyboardInterrupt:
    print("you hit ctrl-c")
    pulses = 500
    pi.set_servo_pulsewidth(M1, pulses)
    pi.set_servo_pulsewidth(M2, pulses)
    pi.set_servo_pulsewidth(M3, pulses)
    pi.set_servo_pulsewidth(M4, pulses)
    pi.stop()
