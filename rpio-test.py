import RPIO
from RPIO import PWM
import time

PIN = 18
s = 1000000
ms = 1000
baseCycle = 8*ms

# init servo
servo = PWM.Servo(0, baseCycle)

rotation = 0

while rotation > -1:
    rotation = float(input("Rotation \n"))
    
    if rotation > -1:
        servo.set_servo(PIN, rotation * 10)
       
    print("Rotation: %d" % rotation)

print("...end")


# Clear servo 
servo.stop_servo(18)
